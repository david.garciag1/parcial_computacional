import numpy as np
import matplotlib.pyplot as plt

class ODE_SOLUTIONS:
    
    def __init__(self, t0, tf):
        
        self.a=t0
        self.b=tf

    def Euler(self,x0,y0,f, n):
        h=(self.b-x0)/n
        F=[[x0],[y0]]
        for i in np.arange(x0,self.b+h,h):
            F[0].append(x0+h)
            F[1].append(f(x0,y0)*h+y0)
            x0=x0+h
            y0=f(x0,y0)*h+y0
        return F
    
    def EulerM(self,x0,y0,f, n):

        h=(self.b-x0)/n
        F=[[x0],[y0]]
        xb=x0+h
        yb=y0+f(x0,y0)*h

        for i in np.arange(x0,self.b+h,h):
            F[0].append(x0+h)
            F[1].append(((f(x0,y0)+f(xb,yb))/2)*h+y0)
            x0=x0+h
            y0=((f(x0,y0)+f(xb,yb))/2)*h+y0
            xb=x0+h
            yb=y0+f(x0,y0)*h

        return F
    
    def k_coef(self):
        return
    
    def RKG(self,N,f,h,x0, y0, z0, parameter):

        y_i=np.zeros(N)

        m=0

        for i in np.arange(0,N,1):
            if f == self.f1:
                y_i[i]=x0+(i+1)*(h/N)*f(0,x0,y0,z0,parameter)
            elif f == self.f2:
                y_i[i]=y0+(i+1)*(h/N)*f(0,x0,y0,z0,parameter)
            elif f == self.f3:
                y_i[i]=z0+(i+1)*(h/N)*f(0,x0,y0,z0,parameter)
            #y_i[i]=y0+(i+1)*(h/N)*f(0,x0,y0,z0,parameter) #pendientes para cada punto

        if f == self.f1:
            for k in np.arange(0,N,1):
                m+=f(x0+(k)*(h/N),y_i[k],y0,z0,parameter) #promedio de las pendientes
            m=m/N
            return x0+m*h

        elif f == self.f2:
            for k in np.arange(0,N,1):
                m+=f(y0+(k)*(h/N),x0,y_i[k],z0,parameter)
            m=m/N
            return y0+m*h

        elif f == self.f3:
            for k in np.arange(0,N,1):
                m+=f(z0+(k)*(h/N),x0,y0,y_i[k],parameter)
            m=m/N
            return z0+m*h

class SOLUTION_CDS(ODE_SOLUTIONS):
    def __init__(self, t0, tf, x0, y0, z0, n, sigma, rho, beta):
        super().__init__(t0, tf)
        self.x0=x0
        self.y0=y0
        self.z0=z0
        self.n=n
        self.sigma=sigma
        self.rho=rho
        self.beta=beta

    def f1(self,t,x,y,z,sigma):
        return sigma*(y-x)
    
    def f2(self,t,x,y,z,rho):
        return x*(rho-z)-y
    
    def f3(self,t,x,y,z,beta):
        return x*y-beta*z
    
    def Lorenz(self):
        h=(self.b-self.a)/self.n
        xn=[self.x0]
        yn=[self.y0]
        zn=[self.z0]
        for i in np.arange(0,self.n,1):
            xn.append(self.RKG(4,self.f1,h,xn[-1], yn[-1], zn[-1],self.sigma))
            yn.append(self.RKG(4,self.f2,h,xn[-1], yn[-1], zn[-1],self.rho))
            zn.append(self.RKG(4,self.f3,h,xn[-1], yn[-1], zn[-1],self.beta))
        t = np.arange(self.a,self.b+h,h)
        return t,xn,yn,zn
    
    def plot_RKG_3D(self):

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(self.Lorenz()[1],self.Lorenz()[2],self.Lorenz()[3])
        plt.show()
        plt.savefig("graf.png")