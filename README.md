# Parcial_computacional



## NOTAS IMPORTANTES

Las imagenes llamadas "Error.png" y "x-y_graph.png" corresponden al primer punto.

Las imágenes llamadas "lorentz_i" y "Graphs_i" corresponden a las gráficas 3D y a las gráficas de x vs y, x vs z y y vs z para cada i que corresponden a los valores iniciales de los puntos 1, 2, 3 y 4.


COMO LAS CONDICIONES INICIALES DE 1 Y 3 SON X=0, Y=0, Z=0. LA FUNCIÓN SIEMPRE DA 0, POR ESO LAS IMÁGENES ESTÁN EN BLANCO, NO ES UN ERROR
PERO COMO SON LOS PROBLEMAS PROPUESTOS, SE INCLUYEN EN EL TRABAJO.

En el caso del punto 2 y 4, sí se puede evidenciar el comportamiento de el atractor de lorentz.

